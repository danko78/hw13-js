document.addEventListener('DOMContentLoaded', () => {
    const btnNewTheme = document.querySelector('.new-theme-btn');
    const page = document.getElementById('theme');
    const theme = localStorage.getItem('theme');

    if (theme === 'greenyellow') {
        page.classList.remove('orange');
        page.classList.add('greenyellow');
    } else {
        page.classList.add('orange');
        page.classList.remove('greenyellow');
    }

    btnNewTheme.addEventListener('click', (e) => {
        page.classList.toggle('greenyellow');
        page.classList.toggle('orange');

        if (page.classList.contains('orange')) {
            localStorage.setItem('theme', 'orange');
        } else if (page.classList.contains('greenyellow')) {
            localStorage.setItem('theme', 'greenyellow');
        }
    })
})